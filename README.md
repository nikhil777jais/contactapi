# ContactAPI
## Endpoints

- Signup
- Login
- GetallUser
- GetoneUser
- UpdateUser
- DeleteUser
- GetallContact
- GetoneContact
- UpdateContact
- DeleteContact

[API URL](https://contactapitest.herokuapp.com/login).
