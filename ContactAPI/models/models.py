from sqlalchemy.orm import backref
from ContactAPI import db
import re 
from flask_restful import abort
from flask_bcrypt import Bcrypt
bcrypt = Bcrypt()

#field decorators
def validate_password(f):
   def decorator(user, password1):
      if not password1:
        abort(406, message='Password not provided')
      if not re.match('\d.*[A-Z]|[A-Z].*\d', password1):
        abort(406, message='Password must contain 1 capital letter and 1 number')
      if len(password1) < 8 or len(password1) > 50:
        abort(406, message='Password must be between 8 and 50 characters')
      return f(user, password1)
   return decorator  

def validate_email(f):
   def decorator(user, email1):
      if not email1:
        abort(406, message='No email provided')
      if not re.match("[^@]+@[^@]+\.[^@]+", email1):
        abort(406, message='Provided email is not an email address')
      return f(user, email1)
   return decorator  

class User(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(20),  nullable=False)
  email = db.Column(db.String(120), unique=True, nullable=False)
  phone = db.Column(db.Integer, nullable=False )
  address = db.Column(db.String(60), nullable=False)
  password = db.Column(db.String(60), nullable=False)
  is_admin = db.Column(db.Boolean, default=False)
  contact = db.relationship('Contact', backref='user')
  
  @validate_email
  def set_email(self, email1):
    self.email = email1

  @validate_password
  def set_password(self, password1):
    self.password = bcrypt.generate_password_hash(password1).decode("utf8") #incryption of password

  def set_phone(self, phone1):
    if len(str(phone1)) < 10:
      abort(406, message='Invalid Phone No')
    self.phone = phone1  
      

  def check_password(self, password1): 
    return bcrypt.check_password_hash(self.password, password1)

  def __repr__(self):
    return f"User('{self.name}', '{self.email}')"


class Contact(db.Model):
  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(20), nullable=False)
  email = db.Column(db.String(120), nullable=False)
  phone = db.Column(db.Integer, nullable=False )
  address = db.Column(db.String(60), nullable=False)
  country = db.Column(db.String(20), nullable=False)
  user_id = db.Column(db.Integer, db.ForeignKey("user.id"), nullable=False)

  def __repr__(self):
    return f"Conatct('{self.name}','{self.phone}','{self.user_id}')" 