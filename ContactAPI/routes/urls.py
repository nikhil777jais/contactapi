from ContactAPI import api
from ContactAPI.tasks.views import UserAPI, UserLogin, UserSignUP, ContactAPI

#Url defined here

api.add_resource(UserAPI, '/user', endpoint='getalluser')
api.add_resource(UserAPI, '/user/<int:id>', endpoint='getoneuser')
api.add_resource(UserLogin, '/login', endpoint='login')
api.add_resource(UserSignUP, '/signup', endpoint='signup')
api.add_resource(ContactAPI, '/contact', endpoint='getallcontact')
api.add_resource(ContactAPI, '/contact/<int:id>', endpoint='getonecontact')


#admin2 = User(name="admin2",email="admin2@gmail.com",phone=12345, address="xyz", password="raja@123")
#con2 = Contact(name="con2",email="con2@gmail.com",phone=12345, address="xyz", country="india", user_id=2)