from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_migrate import Migrate
app = Flask(__name__)
from flask_cors import CORS
CORS(app)
app.config.from_object("ContactAPI.configs.config.Configuration")
db = SQLAlchemy(app) 
migrate = Migrate(app, db)
api = Api(app) 

from ContactAPI.routes import urls #to avoid circualar import #looking for URLs