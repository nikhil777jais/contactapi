from ContactAPI import app, db
from ContactAPI.models.models import User, Contact
from ContactAPI.utilities.parsers import user_create_parser, user_login_parser, user_update_parser,contact_create_parser,contact_update_parser
from flask_restful import Resource, marshal_with, abort
from ContactAPI.utilities.serializer import user_fields_serializer, message_serializer, token_serializer,contact_fields_serializer
import datetime
import jwt
from ContactAPI.utilities.decorators import token_required



#API logic
class UserAPI(Resource):
  @token_required
  @marshal_with(user_fields_serializer)
  def get(self, current_user, id=None, ):
    if id:
      user = User.query.get(id)
      if not user:  
        abort(404, message="user with this is does't exist")
      return user, 200
    user = User.query.all()
    return user, 200

  @token_required
  @marshal_with(user_fields_serializer)
  def patch(self, current_user, id):
    args = user_update_parser.parse_args()
    user = User.query.get(id)
    if not user:
      abort(203, message="user not found")
  
    if args.email:#for updating user-email
      user1 = User.query.filter_by(email=args.email).first()
      if user1 and user1 != user:
        abort(406, message="email already exist")
      user.set_email(args.email)
      
    if args.phone:#for updating user-phone
      user.set_phone(args.phone)

    if args.password:#for updating user-password
      user.set_password(args.password)

    db.session.add(user)
    db.session.commit()    
    return user, 201

  @token_required 
  @marshal_with(message_serializer)
  def delete(self, current_user, id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    message = {"message":"deleted"}
    return message , 200  

class UserSignUP(Resource):
  @marshal_with(user_fields_serializer)
  def post(self):
    args = user_create_parser.parse_args()
    if User.query.filter_by(email=args.email).first(): #check if a email alredy exist in db
        abort(406, message="email already exist")
    user = User(name=args.name, address=args.address)
    user.set_email(args.email)
    user.set_phone(args.phone)
    user.set_password(args.password)
    db.session.add(user)
    db.session.commit()
    return user, 201

class UserLogin(Resource):
  @marshal_with(token_serializer)
  def post(self):
    args = user_login_parser.parse_args()
    if not args.email: 
      abort(401, message="email reqiuired")
    if not args.password:
      abort(401, message="password reqiuired")
    user = User.query.filter_by(email=args.email).first()
    if not user:
      abort(404, message="user not found")
    if not user.check_password(args.password):
      abort(404, message="Incorrect Password")

    token = jwt.encode({'id': user.id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=60)}, app.config['SECRET_KEY'])  
    token_obj = {"token":token.decode('utf8')}
    return token_obj, 200

class ContactAPI(Resource):
  @token_required
  @marshal_with(contact_fields_serializer)
  def get(self, current_user, id = None):
    if id:
      contact = Contact.query.filter_by(user_id = self.id, id = id).first()
      if not contact:
        abort(203, message="contact not found")
      return contact, 200  
    contacts = Contact.query.filter_by(user_id = self.id).all()
    return contacts, 200

  @token_required
  @marshal_with(contact_fields_serializer)
  def post(self, current_user):
    args = contact_create_parser.parse_args()
    contact = Contact(name=args.name, email=args.email, phone=args.phone, address=args.address, country=args.country, user=self)
    db.session.add(contact)
    db.session.commit()
    return contact, 201
  
  @token_required
  @marshal_with(contact_fields_serializer)
  def patch(self, current_user, id=None):
    if id:
      contact = Contact.query.filter_by(user_id = self.id, id = id).first()
      if not contact:
        abort(203, message="contact not found")
      args = contact_update_parser.parse_args()
      if args.name: #for updating name 
        contact.name = args.name
      if args.email: #for updating email 
        contact.email = args.email
      if args.phone: #for updating phone 
        contact.phone = args.phone
      if args.address: #for updating address 
          contact.address = args.address
      if args.country: #for updating country 
          contact.country = args.country
      db.session.add(contact)
      db.session.commit()
      return contact, 200
    else:
      abort(203, message="contact id not found")

  @token_required
  @marshal_with(message_serializer)
  def delete(self, current_user, id):
    if not id:
      abort(203, message="contact not found")
    contact = Contact.query.filter_by(user_id = self.id, id = id).first()
    if not contact:
      abort(203, message="contact not found")
    db.session.delete(contact)
    db.session.commit()
    message = {"message":"deleted"}
    return message , 200 