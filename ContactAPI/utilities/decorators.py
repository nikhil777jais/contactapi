from functools import wraps
from ContactAPI import app
from ContactAPI.models.models import User
import jwt
from flask import request, jsonify, make_response

def token_required(f):
   @wraps(f)
   def decorator(*args, **kwargs):
      token = None
      if 'x-access-tokens' in request.headers:
         token = request.headers['x-access-tokens']
         
      if not token:
         data = {'message': 'unauthorized access'}
         return make_response(jsonify(data), 401)
      try:
         data = jwt.decode(token, app.config['SECRET_KEY'])
         current_user = User.query.filter_by(id=data['id']).first()
      except:
         rdata = {'message': 'invalid token'}
         return make_response(jsonify(data), 401) 
      return f(current_user, *args, **kwargs)
   return decorator

 