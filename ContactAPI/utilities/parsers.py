from flask_restful import reqparse

#parser created here
#User Parser
user_create_parser = reqparse.RequestParser()
user_create_parser.add_argument("name", type=str, help="Name of user", required=True)
user_create_parser.add_argument("email", type=str, help="Email of user", required=True)
user_create_parser.add_argument("phone", type=int, help="Phone of user", required=True)
user_create_parser.add_argument("address", type=str, help="address of user", required=True)
user_create_parser.add_argument("password", type=str, help="Password of user", required=True)

user_update_parser = reqparse.RequestParser()
user_update_parser.add_argument("name", type=str)
user_update_parser.add_argument("email", type=str)
user_update_parser.add_argument("phone", type=int)
user_update_parser.add_argument("address", type=str)
user_update_parser.add_argument("password", type=str)

user_login_parser = reqparse.RequestParser()
user_login_parser.add_argument("email", type=str, help="Email of user", required=True)
user_login_parser.add_argument("password", type=str, help="Password of user", required=True)

#Contact Parser
contact_create_parser = reqparse.RequestParser()
contact_create_parser.add_argument("name", type=str, help="Name required", required=True)
contact_create_parser.add_argument("email", type=str, help="Email required", required=True)
contact_create_parser.add_argument("phone", type=int, help="Phone required", required=True)
contact_create_parser.add_argument("address", type=str, help="address required", required=True)
contact_create_parser.add_argument("country", type=str, help="country required", required=True)

contact_update_parser = reqparse.RequestParser()
contact_update_parser.add_argument("name", type=str)
contact_update_parser.add_argument("email", type=str)
contact_update_parser.add_argument("phone", type=int)
contact_update_parser.add_argument("address", type=str)
contact_update_parser.add_argument("country", type=str)

