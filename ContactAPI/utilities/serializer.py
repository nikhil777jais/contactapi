from flask_restful import fields
#user_seralizer

user_fields_serializer = {
  'name': fields.String,
  'email': fields.String,
  'phone': fields.Integer,
  'address': fields.String,
  'password': fields.String,
  'is_admin': fields.Boolean,
}
message_serializer = {
  'message':fields.String,
}

token_serializer = {
  'token':fields.String,
}
contact_fields_serializer = {
  'name': fields.String,
  'email': fields.String,
  'phone': fields.Integer,
  'address': fields.String,
  'country': fields.String,
  'user_id': fields.Integer,
}